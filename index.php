<?php

require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");

echo "output akhir <br>";
echo "<br>";

echo "Name : " . $sheep->name; // "shaun"
echo "<br>";
echo "legs : " . $sheep->legs; // 4
echo "<br>";
echo "cold blooded : " . $sheep->cold_blooded; // "no"
echo "<br. <br>";


$kodok = new Frog("buduk");
echo "<br> <br>";
echo "Name : " . $kodok->name; // ""
echo "<br>";
echo "legs : " . $kodok->legs; // 
echo "<br>";
echo "cold blooded : " . $kodok->cold_blooded; // ""
echo "<br>";
echo " Jump : ";
$kodok->jump(); // hop hop



$sungokong = new Ape("kera sakti");
echo "<br> <br>";
echo "Name : " . $sungokong->name; // ""
echo "<br>";
echo "legs : " . $sungokong->legs; // 
echo "<br>";
echo "cold blooded : " . $sungokong->cold_blooded; // ""
echo "<br>";
echo "Yell : ";
$sungokong->yell(); // "Auooo"
echo "<br>";


// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>